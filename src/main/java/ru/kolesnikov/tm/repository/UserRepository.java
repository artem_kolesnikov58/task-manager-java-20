package ru.kolesnikov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.api.repository.IUserRepository;
import ru.kolesnikov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final @NotNull String id) {
        for (final User user: entities) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final @NotNull User user) {
        entities.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final @NotNull String login) {
        for (final User user: entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeById(final @NotNull String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final @NotNull String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user);
        return removeUser(user);
    }

}
