package ru.kolesnikov.tm.repository;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.api.repository.IProjectRepository;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.exception.system.IndexIncorrectException;
import ru.kolesnikov.tm.exception.system.UnknownIdException;
import ru.kolesnikov.tm.exception.system.UnknownIndexException;
import ru.kolesnikov.tm.exception.system.UnknownNameCommand;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        entities.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project: entities) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAll(userId);
        entities.removeAll(projects);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Project project: entities) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        throw new UnknownIdException();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        for (@NotNull final Project project: entities) {
            if (!userId.equals(project.getUserId())) continue;
            if (entities.indexOf(project) == index) return project;
        }
        throw new UnknownIndexException();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Project project: entities) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        throw new UnknownNameCommand();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Project project = findOneById(userId, id);
        entities.remove(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Project project = findOneByIndex(userId, index);
        entities.remove(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeOneByName(@NotNull final String userId, @NotNull final String name)  {
        @NotNull final Project project = findOneByName(userId, name);
        entities.remove(project);
        return project;
    }

}