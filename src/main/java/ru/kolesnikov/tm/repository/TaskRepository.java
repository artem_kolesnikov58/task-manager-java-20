package ru.kolesnikov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.api.repository.ITaskRepository;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.exception.system.UnknownIdException;
import ru.kolesnikov.tm.exception.system.UnknownIndexException;
import ru.kolesnikov.tm.exception.system.UnknownNameCommand;
import ru.kolesnikov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(final @NotNull String userId, final @NotNull Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull Task task) {
        entities.remove(task);
    }

    @Override
    public @NotNull List<Task> findAll(final @NotNull String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: entities) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        entities.clear();
    }

    @Override
    public @NotNull Task findOneById(final @NotNull String userId, final @NotNull String id) {
        for (@NotNull final Task task: entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        throw new UnknownIdException();
    }

    @Override
    public @NotNull Task removeOneById(final @NotNull String userId, final @NotNull String id) {
        Task task = findOneById(userId, id);
        entities.remove(task);
        return task;
    }

    @Override
    public @NotNull Task findOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        for (@NotNull final Task task: entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (entities.indexOf(task) == index) return task;
        }
        throw new UnknownIndexException();
    }

    @Override
    public @NotNull Task findOneByName(final @NotNull String userId, final @NotNull String name) {
        for (@NotNull final Task task: entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        throw new UnknownNameCommand();
    }

    @Override
    public @NotNull Task removeOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final Task task = findOneByIndex(userId, index);
        entities.remove(task);
        return task;
    }

    @Override
    public @NotNull Task removeOneByName(final @NotNull String userId, final @NotNull String name) {
        final Task task = findOneByName(userId, name);
        entities.remove(task);
        return task;
    }

}