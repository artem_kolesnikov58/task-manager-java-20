package ru.kolesnikov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String userId;

    @NotNull
    @Override
    public String toString() {
        return "Project [" +
                "Name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ']';
    }

}