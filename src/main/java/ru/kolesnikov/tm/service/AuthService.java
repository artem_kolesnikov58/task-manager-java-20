package ru.kolesnikov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.api.service.IAuthService;
import ru.kolesnikov.tm.api.service.IUserService;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.exception.empty.*;
import ru.kolesnikov.tm.exception.user.AccessDeniedException;
import ru.kolesnikov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    @SneakyThrows
    public void updatePassword(String userId, String newPassword) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();
        userService.updatePassword(userId, newPassword);
    }

    @Override
    @SneakyThrows
    public void updateUserFirstName(String userId, String newFirstName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        userService.updateUserFirstName(userId, newFirstName);
    }

    @Override
    @SneakyThrows
    public void updateUserLastName(String userId, String newLastName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        userService.updateUserLastName(userId, newLastName);
    }

    @Override
    @SneakyThrows
    public void updateUserMiddleName(String userId, String newMiddleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        userService.updateUserMiddleName(userId, newMiddleName);
    }

    @Override
    @SneakyThrows
    public void updateUserEmail(String userId, String newEmail) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        userService.updateUserEmail(userId, newEmail);
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    @SneakyThrows
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final String userId = getUserId();
        @Nullable final User user = userService.findById(userId);
        @NotNull final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (@Nullable final Role item: roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void registry(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        userService.create(login, password);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @SneakyThrows
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userService.create(login, password, email);
    }

}