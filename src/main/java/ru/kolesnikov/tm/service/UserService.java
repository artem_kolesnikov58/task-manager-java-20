package ru.kolesnikov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.api.repository.IUserRepository;
import ru.kolesnikov.tm.api.service.IUserService;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.exception.empty.*;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.HashUtil;

import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @SneakyThrows
    @Override
    public User updatePassword(final String userId, final String newPassword) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();
        final String passwordHash = HashUtil.getHashLine(newPassword);
        final User user = findById(userId);
        user.setPasswordHash(passwordHash);
        return user;
    }

    @Override
    @SneakyThrows
    public User updateUserFirstName(final String userId, final String newFirstName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        final User user = findById(userId);
        user.setFirstName(newFirstName);
        return user;
    }

    @Override
    @SneakyThrows
    public User updateUserLastName(final String userId, final String newLastName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        final User user = findById(userId);
        user.setLastName(newLastName);
        return user;
    }

    @Override
    @SneakyThrows
    public User updateUserMiddleName(final String userId, final String newMiddleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        final User user = findById(userId);
        user.setMiddleName(newMiddleName);
        return user;
    }

    @Override
    @SneakyThrows
    public User updateUserEmail(final String userId, final String newEmail) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        final User user = findById(userId);
        user.setEmail(newEmail);
        return user;
    }

    @Override
    @SneakyThrows
    public User create(final String login, final String password, final String email, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setEmail(email);
        user.setRole(role);
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        return userRepository.removeByLogin(login);
    }

    @Override
    @SneakyThrows
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    @SneakyThrows
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @SneakyThrows
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

}