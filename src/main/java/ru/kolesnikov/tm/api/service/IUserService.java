package ru.kolesnikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.enumerated.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    @Nullable
    User updatePassword(@Nullable String userId, @Nullable String newPassword);

    @Nullable
    User updateUserFirstName(@Nullable String userId, @Nullable String newFirstName);

    @Nullable
    User updateUserLastName(@Nullable String userId, @Nullable String newLastName);

    @Nullable
    User updateUserMiddleName(@Nullable String userId, @Nullable String newMiddleName);

    @Nullable
    User updateUserEmail(@Nullable String userId, @Nullable String newEmail);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeUser(@Nullable User user);

    @Nullable
    User removeById(@Nullable String id);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

}