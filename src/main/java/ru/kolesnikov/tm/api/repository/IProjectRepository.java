package ru.kolesnikov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeOneByName(@NotNull String userId, @NotNull String name);

}