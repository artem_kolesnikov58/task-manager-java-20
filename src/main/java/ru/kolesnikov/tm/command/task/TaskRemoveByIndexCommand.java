package ru.kolesnikov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = serviceLocator.getTaskService().removeOneByIndex(userId, index);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}