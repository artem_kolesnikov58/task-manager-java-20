package ru.kolesnikov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandName() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Artem Kolesnikov");
        System.out.println("E-MAIL: tema58-rus@yandex.ru");
    }

}