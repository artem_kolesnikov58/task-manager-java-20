package ru.kolesnikov.tm.command.data.json.jaxb;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public class DataJsonJaxbClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-json-jb-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json jax-b file.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE JSON JAX-B FILE]");
        @NotNull final File file = new File(DataConstant.FILE_JSON_JB);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}