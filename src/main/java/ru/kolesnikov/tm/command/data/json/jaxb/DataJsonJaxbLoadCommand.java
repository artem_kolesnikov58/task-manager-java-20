package ru.kolesnikov.tm.command.data.json.jaxb;

import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJsonJaxbLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-json-jb-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json jax-b file.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON JAX-B LOAD]");

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File(DataConstant.FILE_JSON_JB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);

        serviceLocator.getDomainService().load(domain);

        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGIN AGAIN]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}