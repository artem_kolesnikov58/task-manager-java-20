package ru.kolesnikov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("Пользователь"),
    ADMIN("Администратор");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}