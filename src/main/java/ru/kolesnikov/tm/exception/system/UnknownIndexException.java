package ru.kolesnikov.tm.exception.system;

import ru.kolesnikov.tm.exception.AbstractException;

public final class UnknownIndexException extends AbstractException {

    public UnknownIndexException() {
        super("Error! Unknown index...");
    }

}