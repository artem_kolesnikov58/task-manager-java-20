package ru.kolesnikov.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractException(@NotNull String message) {
        super(message);
    }

}